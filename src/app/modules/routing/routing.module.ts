import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { AuthComponent } from 'src/app/pages/auth/auth.component';
import { DashboardComponent } from 'src/app/pages/dashboard/dashboard.component';
import { PageNotFoundComponent } from 'src/app/pages/page-not-found/page-not-found.component';

// Guards
import { AuthGuard } from '../../guards/auth.guard';
import { AuthComponentGuard } from 'src/app/guards/auth-component.guard';
import { ClientsComponent } from 'src/app/pages/clients/clients.component';
import { QuotesComponent } from 'src/app/pages/quotes/quotes.component';
import { OrdersComponent } from 'src/app/pages/orders/orders.component';
import { AdminGuard } from 'src/app/guards/admin.guard';
import { StatisticsComponent } from 'src/app/pages/statistics/statistics.component';
import { ManagementComponent } from 'src/app/pages/management/management.component';

import { EmployeesManagementComponent } from 'src/app/pages/management/employees-management/employees-management.component';
import { CompaniesManagementComponent } from 'src/app/pages/management/companies-management/companies-management.component';
import { RightsManagementComponent } from 'src/app/pages/management/rights-management/rights-management.component';
import { FramesManagementComponent } from 'src/app/pages/management/frames-management/frames-management.component';
import { ParametersManagementComponent } from 'src/app/pages/management/parameters-management/parameters-management.component';
import { GeneralParametersManagementComponent } from 'src/app/pages/management/general-parameters-management/general-parameters-management.component';
import { ComponentsManagementComponent } from 'src/app/pages/management/components-management/components-management.component';
import { SuppliersManagementComponent } from 'src/app/pages/management/suppliers-management/suppliers-management.component';
import { InstallationsComponent } from 'src/app/pages/installations/installations.component';

const routes: Routes = [
  { path: 'auth', component: AuthComponent, canActivate: [AuthComponentGuard] },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard],
  },
  { path: 'clients', component: ClientsComponent, canActivate: [AuthGuard] },
  {
    path: 'installations',
    component: InstallationsComponent,
    canActivate: [AuthGuard],
  },
  { path: 'quotes', component: QuotesComponent, canActivate: [AuthGuard] },
  { path: 'orders', component: OrdersComponent, canActivate: [AuthGuard] },
  {
    path: 'management',
    canActivate: [AuthGuard, AdminGuard],
    children: [
      { path: 'employees', component: EmployeesManagementComponent },
      { path: 'companies', component: CompaniesManagementComponent },
      { path: 'rights', component: RightsManagementComponent },
      { path: 'frames', component: FramesManagementComponent },
      { path: 'parameters', component: ParametersManagementComponent },
      {
        path: 'general-parameters',
        component: GeneralParametersManagementComponent,
      },
      { path: 'components', component: ComponentsManagementComponent },
      { path: 'suppliers', component: SuppliersManagementComponent },
      { path: '', component: ManagementComponent },
    ],
  },
  {
    path: 'statistics',
    component: StatisticsComponent,
    canActivate: [AuthGuard],
  },
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class RoutingModule {}
