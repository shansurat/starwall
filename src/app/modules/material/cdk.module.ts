import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Material CDK Modules
import { OverlayModule } from '@angular/cdk/overlay';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { LayoutModule } from '@angular/cdk/layout';

@NgModule({
  declarations: [],
  imports: [CommonModule, OverlayModule, DragDropModule, LayoutModule],
  exports: [OverlayModule, DragDropModule, LayoutModule],
})
export class MaterialCdkModule {}
