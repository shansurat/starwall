import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Right } from '../interfaces/right';

@Injectable({
  providedIn: 'root',
})
export class RightsService {
  public rights$: BehaviorSubject<Right[]> = new BehaviorSubject<Right[]>([]);

  constructor(private http: HttpClient) {
    this.http
      .get('https://next.json-generator.com/api/json/get/NyCUzACG5')
      .subscribe((rights) => this.rights$.next(rights as Right[]));
  }
}
