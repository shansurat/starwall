import { query } from '@angular/animations';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore'
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import firebase from 'firebase/app'
import { Router } from '@angular/router';

interface SignInFormValue {
  username: string;
  password: string;
  remember_me: boolean
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  currentUser$: Observable<firebase.User | null>

  constructor(
    public auth: AngularFireAuth,
    private firestore: AngularFirestore,
    private router: Router
  ) {

    this.currentUser$ = this.auth.user.pipe(map(user => user))
  }


  signIn(signInFormValue: SignInFormValue) {

    this.auth
      .setPersistence(signInFormValue ? firebase.auth.Auth.Persistence.LOCAL : firebase.auth.Auth.Persistence.SESSION)
      .then(() => {
        return this.getEmail(signInFormValue.username)
          .subscribe(email => {
            return this.auth.signInWithEmailAndPassword(email, signInFormValue.password)
              .then(user => {
                this.router.navigate(['dashboard'])
                return user.user
              })
              .catch(e => e)
              .finally(() => { console.log("SignIn Done!") })
          })
      })
      .catch(e => e)


    return true
  }

  signOut() {
    console.log("Signing Out")
    this.auth.signOut()
      .then(() => {
        this.router.navigate(['auth'])
      })
  }

  // SHOULD BE IN FIREBASE FUNCTIONS!!!
  getEmail(username: string): Observable<string> {
    return this.firestore.collection('users', user => user.where('username', '==', username)).get()
      .pipe(map(querySnapshot => {
        if (querySnapshot.size) {
          let account_details: any = querySnapshot.docs[0].data();

          return account_details?.email;
        }

        return null;
      }))
  }

}
