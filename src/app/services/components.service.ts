import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { StarwallComponent } from '../interfaces/component';

@Injectable({
  providedIn: 'root',
})
export class ComponentsService {
  public components$: BehaviorSubject<
    StarwallComponent[]
  > = new BehaviorSubject<StarwallComponent[]>([]);

  constructor(private http: HttpClient) {
    this.http
      .get('https://next.json-generator.com/api/json/get/N1TdkH1Q9')
      .subscribe((components) =>
        this.components$.next(components as StarwallComponent[])
      );
  }
}
