import { Injectable } from '@angular/core';
import { from, Observable } from 'rxjs';
import { MapsAPILoader } from '@agm/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AddressService {
  constructor(public mapsAPILoader: MapsAPILoader) {}
}
