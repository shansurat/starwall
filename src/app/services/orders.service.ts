import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Order } from '../interfaces/order';

@Injectable({
  providedIn: 'root',
})
export class OrdersService {
  public orders$: BehaviorSubject<Order[]> = new BehaviorSubject<Order[]>([]);

  constructor(private http: HttpClient) {
    this.http
      .get('https://next.json-generator.com/api/json/get/VJrBD4c-c')
      .subscribe((order) => this.orders$.next(order as Order[]));
  }
}
