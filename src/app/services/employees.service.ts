import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Employee } from '../interfaces/employee';

@Injectable({
  providedIn: 'root',
})
export class EmployeesService {
  public employees$: BehaviorSubject<Employee[]> = new BehaviorSubject<
    Employee[]
  >([]);

  constructor(private http: HttpClient) {
    this.http
      .get('https://next.json-generator.com/api/json/get/EyelX5RM9')
      .subscribe((employees) => this.employees$.next(employees as Employee[]));
  }
}
