import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Company } from '../interfaces/company';

@Injectable({
  providedIn: 'root',
})
export class CompaniesService {
  public companies$: BehaviorSubject<Company[]> = new BehaviorSubject<
    Company[]
  >([]);

  constructor(private http: HttpClient) {
    this.http
      .get('https://next.json-generator.com/api/json/get/4kwVbysf9')
      .subscribe((companies) => this.companies$.next(companies as Company[]));
  }
}
