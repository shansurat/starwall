import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, from } from 'rxjs';
import { Quote } from '../interfaces/quote';
import firebase from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
import { Representative } from '../interfaces/representative';
import { Company } from '../interfaces/company';

@Injectable({
  providedIn: 'root',
})
export class QuotesService {
  public quotes$: BehaviorSubject<Quote[]> = new BehaviorSubject<Quote[]>([]);

  constructor(
    private http: HttpClient,
    private afs: AngularFirestore
  ) {
    afs
      .collection('quotes')
      .valueChanges()
      .subscribe((clients) => {
        return this.quotes$.next(clients as Quote[])
      }
      );
  }

  generateQuoteId() {
    let today = new Date();

    let id = `${('0' + today.getDay()).slice(-2)}-${(
      '0' +
      (today.getMonth() + 1)
    ).slice(-2)}-001`;

    console.log(id);
    return id;
  }

  async addQuote(val: any, id: string, update: boolean) {
    let quote = {
      id,
      ...val,
      lastModifiedAt: firebase.firestore.FieldValue.serverTimestamp(),
    };

    if (!update)
      quote.createdAt = firebase.firestore.FieldValue.serverTimestamp();

    quote.quoteRequest.representative = ({
      name: quote.quoteRequest.representative,
    } as unknown) as Representative;
    quote.quoteRequest.company = ({
      name: quote.quoteRequest.company,
    } as unknown) as Company;

    console.log(quote)

    return this.afs.collection('quotes').doc(id).set(quote, { merge: true });

  }

  deleteQuote(id: string) {
    return from(this.afs.collection("clients").doc(id).delete())
  }
}
