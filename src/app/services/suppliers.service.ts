import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Supplier } from '../interfaces/supplier';

@Injectable({
  providedIn: 'root',
})
export class SuppliersService {
  public suppliers$: BehaviorSubject<Supplier[]> = new BehaviorSubject<
    Supplier[]
  >([]);

  constructor(private http: HttpClient) {
    this.http
      .get('https://next.json-generator.com/api/json/get/EJJ4rblQq')
      .subscribe((suppliers) => this.suppliers$.next(suppliers as Supplier[]));
  }
}
