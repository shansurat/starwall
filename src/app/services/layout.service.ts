import { Injectable } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {
  isHandset$: Observable<any>
  isWeb$: Observable<any>
  isTablet$: Observable<any>

  minWidth: (width: number) => Observable<boolean>
  maxWidth: (width: number) => Observable<boolean>

  constructor(breakpointObserver: BreakpointObserver) {
    this.isHandset$ = breakpointObserver.observe([
      Breakpoints.Handset
    ]).pipe(
      map(res => res.matches),
      distinctUntilChanged()
    )

    this.isWeb$ = breakpointObserver.observe([
      Breakpoints.Web
    ]).pipe(
      map(res => res.matches),
      distinctUntilChanged()
    )

    this.isTablet$ = breakpointObserver.observe([
      Breakpoints.Tablet
    ]).pipe(
      map(res => res.matches),
      distinctUntilChanged()
    )

    this.minWidth = (width: number) => {
      return breakpointObserver
        .observe(`(min-width: ${width}px)`)
        .pipe(
          map(res => res.matches),
          distinctUntilChanged()
        )
    }


    this.maxWidth = (width: number) => {
      return breakpointObserver
        .observe(`(max-width: ${width}px)`)
        .pipe(
          map(res => res.matches),
          distinctUntilChanged()
        )
    }

  }
}
