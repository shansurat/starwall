import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { BehaviorSubject, from } from 'rxjs';
import { Client } from '../interfaces/client';
import firebase from 'firebase/app';
import { Representative } from '../interfaces/representative';
import { AngularFireFunctions } from '@angular/fire/functions';
import { AngularFireStorage } from '@angular/fire/storage';
import { map } from 'rxjs/operators';
import { Company } from '../interfaces/company';

@Injectable({
  providedIn: 'root',
})
export class ClientsService {
  public clients$: BehaviorSubject<Client[]> = new BehaviorSubject<Client[]>(
    []
  );

  constructor(
    private afs: AngularFirestore,
    private fncs: AngularFireFunctions,
    private storage: AngularFireStorage
  ) {
    afs
      .collection('clients')
      .valueChanges()
      .subscribe((clients) => {
        return this.clients$.next(clients as Client[]);
      });
  }

  generateClientId() {
    return this.afs.createId();
  }

  getClient(id: string) {
    return this.afs.collection('clients').doc(id).valueChanges();
  }

  async addClient(val: any, id: string, update: boolean) {
    let client = {
      id,
      ...val,
      lastModifiedAt: firebase.firestore.FieldValue.serverTimestamp(),
    };

    if (!update)
      client.createdAt = firebase.firestore.FieldValue.serverTimestamp();

    client.accountDetails.status = 'prospect';
    client.accountDetails.representative = ({
      name: client.accountDetails.representative,
    } as unknown) as Representative;
    client.accountDetails.distributor = ({
      name: client.accountDetails.distributor,
    } as unknown) as Company;

    client.noteAndReminders.reminders = val.noteAndReminders?.reminders?.map(
      (reminder: Date) => firebase.firestore.Timestamp.fromDate(reminder)
    );

    // Adding business cards to Firebase Storage
    const businessCards = client.basicDetails.contact.businessCards;

    if (businessCards.length) {
      (
        await this.storage
          .ref(`clients/${id}/businessCards`)
          .listAll()
          .toPromise()
      ).items.forEach((item) => item.delete());

      businessCards.forEach(async (businessCard: File) => {
        await this.storage.upload(
          `clients/${id}/businessCards/${businessCard.name}`,
          businessCard,
          {
            contentType: businessCard.type,
          }
        );
      });
    }

    delete client.basicDetails.contact.businessCards;

    return this.afs.collection('clients').doc(id).set(client, { merge: true });
  }

  deleteClient(id: string) {
    // const deleteClient = this.fncs.httpsCallable('deleteClient');

    // return deleteClient({ id })

    return from(this.afs.collection('clients').doc(id).delete());
  }

  async getBusinessCards(clientId: string) {
    let businessCards: File[] = [];

    let ref = this.storage.ref(`clients/${clientId}/businessCards`);

    (await ref.listAll().toPromise()).items.forEach(async (item) => {
      let url = await item.getDownloadURL();
      let metadata = await item.getMetadata();

      let file = new File(
        [await (await fetch((url as unknown) as string)).blob()],
        item.name,
        {
          type: metadata.contentType,
        }
      );

      console.log(file);

      businessCards.push(file);
    });

    console.log('BUSINESS CARDSSSSSSSSSSSSSs');
    console.log(businessCards.length);

    return businessCards;
  }
}
