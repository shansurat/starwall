import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { RoutingModule } from './modules/routing/routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FlexLayoutModule } from '@angular/flex-layout';

// Fire Modules
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import {
  AngularFireFunctionsModule,
  USE_EMULATOR,
  REGION
} from '@angular/fire/functions';

// Material Modules
import { MaterialModule } from './modules/material/material.module';
import { MaterialCdkModule } from './modules/material/cdk.module';

// Charts Module
import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { environment } from 'src/environments/environment';

// Pages
import { AuthComponent } from './pages/auth/auth.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ClientsComponent } from './pages/clients/clients.component';
import { OrdersComponent } from './pages/orders/orders.component';
import { QuotesComponent } from './pages/quotes/quotes.component';
import { ClientComponent } from './dialogs/new-client/new-client.component';
import { StatisticsComponent } from './pages/statistics/statistics.component';
import { NewUserComponent } from './dialogs/new-user/new-user.component';

// Management Componentes
import { EmployeesManagementComponent } from './pages/management/employees-management/employees-management.component';
import { CompaniesManagementComponent } from './pages/management/companies-management/companies-management.component';
import { FramesManagementComponent } from './pages/management/frames-management/frames-management.component';
import { RightsManagementComponent } from './pages/management/rights-management/rights-management.component';
import { SuppliersManagementComponent } from './pages/management/suppliers-management/suppliers-management.component';
import { ParametersManagementComponent } from './pages/management/parameters-management/parameters-management.component';
import { GeneralParametersManagementComponent } from './pages/management/general-parameters-management/general-parameters-management.component';
import { ComponentsManagementComponent } from './pages/management/components-management/components-management.component';

// Pipes
import { ToolbarCasePipe } from './pipes/toolbar-case.pipe';
import { NameCasePipe } from './pipes/name-case.pipe';
import { SidenavCasePipe } from './pipes/sidenav-case.pipe';
import { SummaryComponent } from './pages/dashboard/summary/summary.component';
import { RecentQuotesTableComponent } from './pages/dashboard/recent-quotes-table/recent-quotes-table.component';
import { QuotesTableComponent } from './components/tables/quotes-table/quotes-table.component';
import { OrdersTableComponent } from './components/tables/orders-table/orders-table.component';
import { ClientsTableComponent } from './components/tables/clients-table/clients-table.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { CamelToTitleCasePipe } from './pipes/camel-to-title-case.pipe';
import { AdvancedTableFilterComponent } from './components/tables/advanced-table-filter/advanced-table-filter.component';

// Calendar Module
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { DashboardCalendarComponent } from './pages/dashboard/dashboard-calendar/dashboard-calendar.component';
import { RecentOrdersTableComponent } from './pages/dashboard/recent-orders-table/recent-orders-table.component';

import { AngularSplitModule } from 'angular-split';
import { QuotesStatisticsComponent } from './pages/dashboard/quotes-statistics/quotes-statistics.component';
import { GridsterModule } from 'angular-gridster2';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { NewQuoteComponent } from './dialogs/new-quote/new-quote.component';
import { FloorHeightInputComponent } from './components/custom-form-fields/floor-height-input/floor-height-input.component';

import { NgxDropzoneModule } from 'ngx-dropzone';

import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import { ProductHandlingDimensionsComponent } from './components/custom-form-fields/product-handling-dimensions/product-handling-dimensions.component';
import { ContactsComponent } from './pages/clients/contacts/contacts.component';
import { DeleteClientComponent } from './dialogs/delete-client/delete-client.component';
import { BusinessCardPreviewComponent } from './dialogs/business-card-preview/business-card-preview.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    PageNotFoundComponent,
    DashboardComponent,
    SidenavCasePipe,
    ClientsComponent,
    OrdersComponent,
    QuotesComponent,
    ClientComponent,
    StatisticsComponent,
    NameCasePipe,
    NewUserComponent,
    ToolbarCasePipe,
    EmployeesManagementComponent,
    CompaniesManagementComponent,
    FramesManagementComponent,
    RightsManagementComponent,
    ComponentsManagementComponent,
    SuppliersManagementComponent,
    ParametersManagementComponent,
    GeneralParametersManagementComponent,
    SummaryComponent,
    RecentQuotesTableComponent,
    QuotesTableComponent,
    OrdersTableComponent,
    ClientsTableComponent,
    CamelToTitleCasePipe,
    AdvancedTableFilterComponent,
    DashboardCalendarComponent,
    RecentOrdersTableComponent,
    QuotesStatisticsComponent,
    NewQuoteComponent,
    FloorHeightInputComponent,
    ProductHandlingDimensionsComponent,
    ContactsComponent,
    ClientComponent,
    DeleteClientComponent,
    BusinessCardPreviewComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RoutingModule,
    BrowserAnimationsModule,

    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    GridsterModule,
    AngularSplitModule,
    FormsModule,
    ReactiveFormsModule,

    FlexLayoutModule,

    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule.enablePersistence(),
    AngularFireStorageModule,
    AngularFireFunctionsModule,

    MaterialModule,
    MaterialCdkModule,
    ChartsModule,
    NgxChartsModule,
    NgxDropzoneModule,
    MaterialFileInputModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA_hEmEq1b8td1odz17SONcgK8MdDrMGkk',
      libraries: ['places'],
    }),
    MatGoogleMapsAutocompleteModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
    }),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    { provide: USE_EMULATOR, useValue: ['localhost', 5001] },
    { provide: REGION, useValue: 'europe-west' }
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
