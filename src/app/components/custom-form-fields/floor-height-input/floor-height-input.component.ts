import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Self,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormBuilder,
  FormGroup,
  NgControl,
  Validators,
} from '@angular/forms';
import { MatFormFieldControl } from '@angular/material/form-field';
import { Subject } from 'rxjs';

class FloorHeight {
  constructor(
    public feet: number | null,
    public inches: number | null,
    public fraction: number | null
  ) {}
}

@Component({
  selector: 'floor-height-input',
  templateUrl: './floor-height-input.component.html',
  styleUrls: ['./floor-height-input.component.scss'],
  providers: [
    { provide: MatFormFieldControl, useExisting: FloorHeightInputComponent },
  ],
})
export class FloorHeightInputComponent
  implements MatFormFieldControl<FloorHeight>, ControlValueAccessor, OnDestroy {
  floorHeightFormGroup!: FormGroup;

  @ViewChild('feetInput') feetInput!: HTMLInputElement;
  @ViewChild('inchesInput') inchesInput!: HTMLInputElement;
  @ViewChild('fractionInput') fractionInput!: HTMLInputElement;

  @Input()
  get value(): FloorHeight | null {
    let val = this.floorHeightFormGroup.value;

    if (val.feet || val.inches) {
      return new FloorHeight(val.feet, val.inches, val.fraction);
    }

    return null;
  }
  set value(tel: FloorHeight | null) {
    const { feet, inches, fraction } = tel || new FloorHeight(null, null, null);
    this.floorHeightFormGroup.setValue({ feet, inches, fraction });
    this.stateChanges.next();
  }

  stateChanges = new Subject<void>();

  static nextId = 0;
  @HostBinding()
  id = `floor-height-input-${FloorHeightInputComponent.nextId++}`;

  onChange = (_: any) => {};
  onTouched = () => {};

  @Input()
  get placeholder() {
    return this._placeholder;
  }
  set placeholder(plh) {
    this._placeholder = plh;
    this.stateChanges.next();
  }

  private _placeholder!: string;

  focused = false;

  get empty() {
    let val = this.floorHeightFormGroup.value;
    return !val.feet && !val.inches && !val.fraction;
  }

  @HostBinding('class.floating')
  get shouldLabelFloat() {
    return this.focused || !this.empty;
  }

  private _required = false;

  @Input()
  get required() {
    return this._required;
  }
  set required(req) {
    this._required = coerceBooleanProperty(req);
    this.stateChanges.next();
  }

  private _disabled = false;
  @Input()
  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this._disabled
      ? this.floorHeightFormGroup.disable()
      : this.floorHeightFormGroup.enable();
    this.stateChanges.next();
  }

  get errorState(): boolean {
    return this.floorHeightFormGroup.invalid && this.floorHeightFormGroup.dirty;
  }

  controlType = 'floor-height-input';

  @Input('aria-describedby') userAriaDescribedBy!: string;
  setDescribedByIds(ids: string[]) {
    const controlElement = this._elementRef.nativeElement.querySelector(
      '.floor-height-input-container'
    )!;
    controlElement.setAttribute('aria-describedby', ids.join(' '));
  }

  onContainerClick(event: MouseEvent) {
    if ((event.target as Element).tagName.toLowerCase() != 'input') {
      this._elementRef.nativeElement.querySelector('input').focus();
    }
  }

  constructor(
    _formBuilder: FormBuilder,
    private _focusMonitor: FocusMonitor,
    private _elementRef: ElementRef,
    @Optional() @Self() public ngControl: NgControl
  ) {
    if (this.ngControl != null) {
      this.ngControl.valueAccessor = this;
    }

    this.floorHeightFormGroup = _formBuilder.group({
      feet: [null],
      inches: [null, [Validators.min(0), Validators.max(11)]],
      fraction: '',
    });

    _focusMonitor.monitor(_elementRef, true).subscribe((origin) => {
      this.focused = !!origin;
      this.stateChanges.next();
    });
  }

  autoFocusNext(
    control: AbstractControl,
    nextElement?: HTMLInputElement
  ): void {
    if (!control.errors && nextElement) {
      this._focusMonitor.focusVia(nextElement, 'program');
    }
  }

  autoFocusPrev(control: AbstractControl, prevElement: HTMLInputElement): void {
    if (control.value.length < 1) {
      this._focusMonitor.focusVia(prevElement, 'program');
    }
  }

  writeValue(tel: FloorHeight | null): void {
    this.value = tel;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  ngOnDestroy() {
    this._focusMonitor.stopMonitoring(this._elementRef);
  }
}
