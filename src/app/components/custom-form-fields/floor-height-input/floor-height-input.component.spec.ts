import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FloorHeightInputComponent } from './floor-height-input.component';

describe('FloorHeightInputComponent', () => {
  let component: FloorHeightInputComponent;
  let fixture: ComponentFixture<FloorHeightInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FloorHeightInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FloorHeightInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
