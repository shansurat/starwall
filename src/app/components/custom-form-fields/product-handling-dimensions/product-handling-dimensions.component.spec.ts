import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductHandlingDimensionsComponent } from './product-handling-dimensions.component';

describe('ProductHandlingDimensionsComponent', () => {
  let component: ProductHandlingDimensionsComponent;
  let fixture: ComponentFixture<ProductHandlingDimensionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductHandlingDimensionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductHandlingDimensionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
