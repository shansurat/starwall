import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnDestroy,
  Optional,
  Self,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormBuilder,
  FormGroup,
  NgControl,
  Validators,
} from '@angular/forms';
import { MatFormFieldControl } from '@angular/material/form-field';
import { Subject } from 'rxjs';

class ProductHandlingDimensions {
  constructor(
    public length: number | null,
    public width: number | null,
    public height: number | null
  ) {}
}

@Component({
  selector: 'product-handling-dimensions-input',
  templateUrl: './product-handling-dimensions.component.html',
  styleUrls: ['./product-handling-dimensions.component.scss'],
  providers: [
    {
      provide: MatFormFieldControl,
      useExisting: ProductHandlingDimensionsComponent,
    },
  ],
})
export class ProductHandlingDimensionsComponent
  implements
    MatFormFieldControl<ProductHandlingDimensions>,
    ControlValueAccessor,
    OnDestroy {
  productHandlingDimensionsFormGroup!: FormGroup;

  @ViewChild('lengtInput') lengtInput!: HTMLInputElement;
  @ViewChild('widthInput') widthInput!: HTMLInputElement;
  @ViewChild('heightInput') heightInput!: HTMLInputElement;

  stateChanges = new Subject<void>();

  @Input()
  get value(): ProductHandlingDimensions | null {
    let val = this.productHandlingDimensionsFormGroup.value;

    return new ProductHandlingDimensions(val.length, val.width, val.height);
  }
  set value(phd: ProductHandlingDimensions | null) {
    const { length, width, height } =
      phd || new ProductHandlingDimensions(null, null, null);
    this.productHandlingDimensionsFormGroup.setValue({
      length,
      width,
      height,
    });
    this.stateChanges.next();
  }

  static nextId = 0;
  @HostBinding()
  id = `product-handling-dimensions-input-${ProductHandlingDimensionsComponent.nextId++}`;

  onChange = (_: any) => {};
  onTouched = () => {};

  private _placeholder!: string;
  @Input()
  get placeholder() {
    return this._placeholder;
  }
  set placeholder(plh) {
    this._placeholder = plh;
    this.stateChanges.next();
  }

  focused = false;

  get empty() {
    let val = this.productHandlingDimensionsFormGroup.value;
    return !val.length && !val.width && !val.height;
  }

  @HostBinding('class.floating')
  get shouldLabelFloat() {
    return this.focused || !this.empty;
  }

  private _required = false;
  @Input()
  get required() {
    return this._required;
  }
  set required(req) {
    this._required = coerceBooleanProperty(req);
    this.stateChanges.next();
  }

  private _disabled = false;
  @Input()
  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this._disabled
      ? this.productHandlingDimensionsFormGroup.disable()
      : this.productHandlingDimensionsFormGroup.enable();
    this.stateChanges.next();
  }

  get errorState(): boolean {
    return (
      this.productHandlingDimensionsFormGroup.invalid &&
      this.productHandlingDimensionsFormGroup.dirty
    );
  }

  controlType = 'product-handling-dimensions-input';

  @Input('aria-describedby') userAriaDescribedBy!: string;
  setDescribedByIds(ids: string[]) {
    const controlElement = this._elementRef.nativeElement.querySelector(
      '.product-handling-dimensions-input-container'
    )!;
    controlElement.setAttribute('aria-describedby', ids.join(' '));
  }

  onContainerClick(event: MouseEvent) {
    if ((event.target as Element).tagName.toLowerCase() != 'input') {
      this._elementRef.nativeElement.querySelector('input').focus();
    }
  }

  constructor(
    _formBuilder: FormBuilder,
    private _focusMonitor: FocusMonitor,
    private _elementRef: ElementRef,
    @Optional() @Self() public ngControl: NgControl
  ) {
    if (this.ngControl != null) {
      this.ngControl.valueAccessor = this;
    }

    this.productHandlingDimensionsFormGroup = _formBuilder.group({
      length: '',
      width: '',
      height: '',
    });

    _focusMonitor.monitor(_elementRef, true).subscribe((origin) => {
      this.focused = !!origin;
      this.stateChanges.next();
    });
  }

  autoFocusNext(
    control: AbstractControl,
    nextElement?: HTMLInputElement
  ): void {
    if (!control.errors && nextElement) {
      this._focusMonitor.focusVia(nextElement, 'program');
    }
  }

  autoFocusPrev(control: AbstractControl, prevElement: HTMLInputElement): void {
    if (control.value.length < 1) {
      this._focusMonitor.focusVia(prevElement, 'program');
    }
  }

  writeValue(phd: ProductHandlingDimensions | null): void {
    this.value = phd;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  ngOnDestroy() {
    this._focusMonitor.stopMonitoring(this._elementRef);
  }
}
