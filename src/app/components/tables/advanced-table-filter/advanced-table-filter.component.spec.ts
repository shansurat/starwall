import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancedTableFilterComponent } from './advanced-table-filter.component';

describe('AdvancedTableFilterComponent', () => {
  let component: AdvancedTableFilterComponent;
  let fixture: ComponentFixture<AdvancedTableFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdvancedTableFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedTableFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
