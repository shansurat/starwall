import { SelectionModel } from '@angular/cdk/collections';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import {
  AfterViewInit,
  Component,
  Input,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Order } from 'src/app/interfaces/order';
import { OrdersTableConfig } from 'src/app/interfaces/orders-table-config';

@Component({
  selector: 'orders-table',
  templateUrl: './orders-table.component.html',
  styleUrls: ['./orders-table.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class OrdersTableComponent implements OnInit, AfterViewInit {
  @Input('config') config!: OrdersTableConfig;
  @Input('orders') orders$!: Order[];

  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  displayedColumns!: string[];

  dataSource!: MatTableDataSource<Order>;
  selection = new SelectionModel<Order>(true, []);

  constructor() {}

  ngOnInit(): void {
    this.displayedColumns = this.config.displayedColumns;
    if (this.config.actionsVisible)
      this.config.displayedColumns.push('actions');
    if (this.config.canSelectRows)
      this.config.displayedColumns.unshift('select');

    this.config.displayedColumns.push('more');
    this.dataSource = new MatTableDataSource(this.orders$);
  }

  ngAfterViewInit() {
    if (this.config.sortable) {
      this.dataSource.sort = this.sort;
      this.dataSource.sortingDataAccessor = (item: any, prop) => {
        if (prop == 'client' || prop == 'company') return item[prop].name;

        return item[prop];
      };
    }

    if (this.config.paginator) this.dataSource.paginator = this.paginator;
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(
      this.config.displayedColumns,
      event.previousIndex,
      event.currentIndex
    );
  }

  // Get Status Color
  getStatusColor(status: string) {
    const statusColors: any = {
      'in-process': '#f57f17',
      'to-process': '#0d47a1',
      completed: '#1b5e20',
      standby: '#b71c1c',
    };

    return statusColors[status];
  }

  //
  // ROW SELECTION FEATURE

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
      row.position + 1
    }`;
  }
}
