import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { Client } from 'src/app/interfaces/client';
import { ClientsService } from 'src/app/services/clients.service';

@Component({
  selector: 'app-delete-client',
  templateUrl: './delete-client.component.html',
  styleUrls: ['./delete-client.component.scss'],
})
export class DeleteClientComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { clientId: string },
    private _matDialogRef: MatDialogRef<DeleteClientComponent>,
    private clientsServ: ClientsService
  ) { }

  ngOnInit(): void { }

  deleteProspect() {
    this._matDialogRef.close(true);
  }
}
