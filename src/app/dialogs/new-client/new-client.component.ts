import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Client } from 'src/app/interfaces/client';
import { ClientsService } from 'src/app/services/clients.service';
import { DeleteClientComponent } from '../delete-client/delete-client.component';
import firebase from 'firebase/app';
import { BusinessCardPreviewComponent } from '../business-card-preview/business-card-preview.component';

interface NewClientDialogData {
  expanded: boolean;
}

@Component({
  selector: 'app-new-client',
  templateUrl: './new-client.component.html',
  styleUrls: ['./new-client.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ClientComponent implements OnInit {
  newProspectFormGroup!: FormGroup;

  fullName!: Observable<string>;

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  clientId!: string;

  defaultTags = [
    'Architect',
    'Construction Leader',
    'Government',
    'Designer',
    'Client utilisateur',
  ];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { client?: Client },
    public matDialogRef: MatDialogRef<ClientComponent>,
    private _formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private _matDialog: MatDialog,
    private clientsServ: ClientsService
  ) { }


  businessCards: File[] = [];

  onSelect(event: any) {
    this.businessCards.push(...event.addedFiles);
    this.newProspectFormGroup.markAsDirty()
  }

  onRemove(event: any) {
    this.businessCards.splice(this.businessCards.indexOf(event), 1);
    this.newProspectFormGroup.markAsDirty()
  }

  ngOnInit(): void {
    this.clientId = this.data?.client?.id
      ? this.data.client.id
      : this.clientsServ.generateClientId();

    if (this.data?.client?.id) {
      this.clientsServ.getBusinessCards(this.data?.client?.id)
        .then(businessCards => this.businessCards = businessCards)
    }

    this.matDialogRef.disableClose = true;

    this.newProspectFormGroup = this._formBuilder.group({
      basicDetails: this._formBuilder.group({
        fullName: this._formBuilder.group({
          firstName: [this.data.client?.basicDetails.fullName.firstName, Validators.required],
          lastName: [this.data.client?.basicDetails.fullName.lastName, Validators.required],
          middleName: this.data.client?.basicDetails.fullName?.middleName,
          suffix: this.data.client?.basicDetails.fullName?.suffix,
        }),
        contact: this._formBuilder.group({
          phoneNumber: [this.data.client?.basicDetails.contact.phoneNumber, Validators.required],
          email: [this.data.client?.basicDetails.contact.email, Validators.required],
        }),
        address: this._formBuilder.group({
          fullAddress: this.data.client?.basicDetails.address.fullAddress,
        }),
      }),

      accountDetails: this._formBuilder.group({
        distributor: [this.data.client?.accountDetails.distributor.name, Validators.required],
        representative: [this.data.client?.accountDetails.representative.name, Validators.required],
        tags: new FormControl(this.data.client?.accountDetails.tags || []),
      }),

      noteAndReminders: this._formBuilder.group({
        note: this.data.client?.noteAndReminders?.note,
        reminders: this.generateRemindersFormArray(this.data.client?.noteAndReminders?.reminders)
      }),

      taxes: this.generateTaxesFormArray(this.data.client?.taxes),

      quoteDetails: this._formBuilder.group({
        quote: this.data.client?.quoteDetails?.quote,
        claimCredit: this.data.client?.quoteDetails?.claimCredit,
        note: this.data.client?.quoteDetails?.note,
      }),

      contacts: this.generateContactsFormArray(this.data.client?.contacts),
    });

    this.fullName = this.newProspectFormGroup
      .get('basicDetails.fullName')
      ?.valueChanges.pipe(
        map(
          (val) => `${val.firstName}${val.lastName ? ' ' + val.lastName : ''}`
        )
      ) as Observable<string>;
  }

  generateFullName(fullname: any) {
    return `${fullname.firstName} ${fullname.lastName}${fullname.suffix ? " " + fullname.suffix : ""}`
  }

  toFormGroup(abstractControl: AbstractControl) {
    return abstractControl as FormGroup;
  }

  toFormControl(abstractControl: AbstractControl): FormControl {
    return abstractControl as FormControl;
  }

  get client() {
    return this.data.client;
  }

  get accountDetails(): FormGroup {
    return this.newProspectFormGroup.get('accountDetails') as FormGroup;
  }

  get noteAndReminders(): FormGroup {
    return this.newProspectFormGroup.get('noteAndReminders') as FormGroup;
  }

  // Reminders Functions
  get reminders(): FormArray {
    return this.noteAndReminders.get('reminders') as FormArray;
  }

  addReminder(): void {
    this.reminders.push(new FormControl('', [Validators.required]));
    this.newProspectFormGroup.markAsDirty()
  }

  removeReminder(i: number): void {
    this.reminders.removeAt(i);
    this.newProspectFormGroup.markAsDirty()
  }

  generateRemindersFormArray(reminders?: firebase.firestore.Timestamp[]) {
    let remindersFormArray: any[] = []

    if (reminders) {
      reminders.forEach(reminder => {
        remindersFormArray.push(new FormControl(reminder.toDate(), [Validators.required]))
      });
    }

    return this._formBuilder.array(remindersFormArray)
  }

  // Taxes Functions

  get taxes(): FormArray {
    return this.newProspectFormGroup.get('taxes') as FormArray;
  }

  addTax(): void {
    this.taxes.push(
      this._formBuilder.group({
        name: [null, Validators.required],
        value: [null, Validators.required],
      })
    );
    this.newProspectFormGroup.markAsDirty()
  }

  removeTax(i: number): void {
    this.taxes.removeAt(i);
    this.newProspectFormGroup.markAsDirty()
  }

  generateTaxesFormArray(taxes?: any[]) {
    let taxesFormArray: any[] = []

    if (taxes) {
      taxes.forEach((tax: any) => {
        taxesFormArray.push(
          this._formBuilder.group({
            name: [tax.name, Validators.required],
            value: [tax.value, Validators.required],
          })
        )
      });
    }

    return this._formBuilder.array(taxesFormArray)
  }

  // Tags Functions

  get tags() {
    return this.newProspectFormGroup.get('accountDetails.tags');
  }
  addTag(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) this.tags!.value.push(value.trim());

    if (input) {
      input.value = '';
    }
    this.newProspectFormGroup.markAsDirty()
  }

  addTagViaAutocomplete(event: MatAutocompleteSelectedEvent) {
    this.tags!.value.push(event.option.value);
    this.newProspectFormGroup.markAsDirty()
  }

  removeTag(tag: string): void {
    const index = this.tags!.value.indexOf(tag);

    if (index >= 0)
      this.tags!.value.splice(index, 1);
    this.newProspectFormGroup.markAsDirty()
  }

  // Contacts Functions

  get contacts() {
    return this.newProspectFormGroup.get('contacts') as FormArray;
  }

  addContact() {
    this.contacts.push(
      this._formBuilder.group({
        name: [null, Validators.required],
        title: [null, Validators.required],
        telephone: [null, Validators.required],
        email: null,
      })
    );
    this.newProspectFormGroup.markAsDirty()
  }

  removeContact(i: number): void {
    this.contacts.removeAt(i);
    this.newProspectFormGroup.markAsDirty()
  }

  generateContactsFormArray(contacts?: any[]) {
    let contactsFormArray: any[] = []

    if (contacts) {
      contacts.forEach((contact: any) => {
        contactsFormArray.push(
          this._formBuilder.group({
            name: [contact.name, Validators.required],
            title: [contact.title, Validators.required],
            telephone: [contact.telephone, Validators.required],
            email: contact?.email,
          })
        )
      });
    }

    return this._formBuilder.array(contactsFormArray)
  }

  discardProspect() {
    this.matDialogRef.close();
    this._snackBar.open('Prospect discarded', '', { duration: 1000 });
  }

  saveProspect() {
    let update = false;
    if (this.data.client?.id) update = true;

    let prospectData = this.newProspectFormGroup.value

    prospectData.basicDetails.contact.businessCards = this.businessCards

    this.matDialogRef.close({
      action: "save",
      value: prospectData,
      clientId: this.clientId,
      update
    })
  }

  deleteProspect() {
    let deleteProspectDialogRef = this._matDialog.open(DeleteClientComponent, {
      data: { clientId: this.clientId },
    });

    deleteProspectDialogRef.afterClosed().subscribe((isDelete) => {
      if (isDelete)
        this.matDialogRef.close({
          action: "delete",
          clientId: this.clientId
        });
    });
  }
}
