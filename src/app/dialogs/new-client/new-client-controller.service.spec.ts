import { TestBed } from '@angular/core/testing';

import { NewClientControllerService } from './new-client-controller.service';

describe('NewClientControllerService', () => {
  let service: NewClientControllerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NewClientControllerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
