import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Client } from 'src/app/interfaces/client';
import { ClientsService } from 'src/app/services/clients.service';
import { ClientComponent } from './new-client.component';

@Injectable({
  providedIn: 'root',
})
export class NewClientControllerService {
  constructor(
    private _matDialog: MatDialog,
    private _snackBar: MatSnackBar,
    private clientsServ: ClientsService
  ) { }

  openClientDialog(client?: Client) {
    let clientDialogRef = this._matDialog.open(ClientComponent, {
      panelClass: 'form-dialog',
      data: { client },
    });

    return clientDialogRef.afterClosed().subscribe((res: any) => {
      if (res) {
        if (res.action == 'save') {
          this.clientsServ
            .addClient(res.value, res.clientId, res.update)
            .then(() =>
              this._snackBar.open('Prospect added', '', { duration: 1000 })
            )
            .catch((err) => this._snackBar.open(err, '', { duration: 1000 }))
        } else if (res.action == 'delete') {
          this.clientsServ.deleteClient(res.clientId).subscribe(() => {
            this._snackBar.open('Prospect deleted', '', { duration: 1000 });
          });
        }
      }
    });
  }
}
