import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-business-card-preview',
  templateUrl: './business-card-preview.component.html',
  styleUrls: ['./business-card-preview.component.scss']
})
export class BusinessCardPreviewComponent implements OnInit {
  businessCardSource: any = "#"

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { url: any },

    public matDialogRef: MatDialogRef<BusinessCardPreviewComponent>
  ) { }

  ngOnInit(): void { }

}
