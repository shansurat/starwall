import { COMMA, ENTER } from '@angular/cdk/keycodes';
import {
  Component,
  ElementRef,
  HostBinding,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  AsyncValidator,
  AsyncValidatorFn,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import {
  MatAutocompleteSelectedEvent,
  MatAutocompleteTrigger,
} from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { MatSnackBar } from '@angular/material/snack-bar';
import { combineLatest, Observable, of } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  last,
  map,
  mergeMap,
  take,
  takeLast,
} from 'rxjs/operators';
import { Client } from 'src/app/interfaces/client';
import { Quote } from 'src/app/interfaces/quote';
import { AddressService } from 'src/app/services/address.service';
import { ClientsService } from 'src/app/services/clients.service';
import { QuotesService } from 'src/app/services/quotes.service';
import { NewClientControllerService } from '../new-client/new-client-controller.service';

interface FloorHeight {
  feet: number | null;
  inches: number | null;
  fraction: number | null;
}

interface ProductHandlingDimensions {
  length: number | null;
  width: number | null;
  height: number | null;
}

const floorColors = [
  'Grey Ral 9006',
  'Milfinish',
  'Clear Anodized',
  'Other Ral Color',
  'Anodized',
];

const workOptions = ['Day', 'Evening', 'Weekend'];

const parkingAvailabilityOptions = [
  'Downtown',
  'Small street',
  'Street parking',
  'Down the project',
  'Inside building',
  'Industrial area',
  'Park',
];

const projectLocationOptions = [
  'Ground floor',
  'Floor',
  'Many floor',
  'Dock',
  'Garbage',
];

const locationDescriptionOptions = [
  {
    name: 'floor',
    options: [
      'Concrete',
      'Wood',
      'Paint',
      'Carpet',
      'Ceramic Floor',
      'Linoleum',
    ],
  },
  {
    name: 'ceiling',
    options: ['Concrete', 'Wood', 'Gypsum', 'Drop ceiling'],
  },
  {
    name: 'walls',
    options: ['Concrete', 'Brick', 'Gypsum'],
  },
  {
    name: 'heating',
    options: ['Underfloor heating', 'Heater', 'Electric baseboard', 'Radiant'],
  },
  {
    name: 'others',
    options: [
      'Sprinkler',
      'General Lighting',
      'Lighting by office',
      'Apparent ventilation',
      'Air conditioning',
    ],
  },
];

@Component({
  selector: 'app-new-quote',
  templateUrl: './new-quote.component.html',
  styleUrls: ['./new-quote.component.scss'],
})
export class NewQuoteComponent implements OnInit {
  @HostBinding('class') class = 'wrapper';
  @ViewChild('clientAutocompleteInput', { read: MatAutocompleteTrigger })
  clientAutocompleteTrigger!: MatAutocompleteTrigger;

  newQuoteFormGroup!: FormGroup;

  expandedView: boolean = false;

  // Default Options
  floorColors = floorColors;
  workOptions = workOptions;
  parkingAvailabilityOptions = parkingAvailabilityOptions;
  projectLocationOptions = projectLocationOptions;
  locationDescriptionOptions = locationDescriptionOptions;

  deliveryAddressSameAsClientAddress: boolean = false;

  readonly separatorKeycodes: number[] = [ENTER, COMMA];

  quoteId!: string;

  clientFilter = new FormControl();
  filteredClients!: Observable<any>;
  filteredContacts!: Observable<any>;

  newQuoteESM = new NewQuoteErrorStateMatcher();

  constructor(
    public matDialogRef: MatDialogRef<NewQuoteComponent>,
    private _formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    public clientsServ: ClientsService,
    public quotesServ: QuotesService,
    public addressServ: AddressService,
    public newClientController: NewClientControllerService,
    @Inject(MAT_DIALOG_DATA) public data: { quote?: Quote }
  ) {
    this.quoteId = quotesServ.generateQuoteId();
  }

  ngOnInit(): void {
    this.newQuoteFormGroup = this._formBuilder.group({
      quoteRequest: this._formBuilder.group({
        company: [
          this.data?.quote?.quoteRequest.company.name,
          Validators.required,
        ],
        client: [
          { value: this.data.quote?.quoteRequest.clientId, disabled: true },
          [Validators.required],
          [clientDNEValidator(this.clientsServ)],
        ],
        contact: [{ value: '', disabled: true }, Validators.required],
        representative: '',
        projectName: [
          `Quote ${this.quotesServ.generateQuoteId()}`,
          Validators.required,
        ],
      }),
      quote: this._formBuilder.group({
        requestedDate: ['', Validators.required],
        installationDate: ['', Validators.required],
        isLocalProject: '',
        distance: 0,
        work: '',
      }),
      floors: this._formBuilder.array([]),
      delivery: this._formBuilder.group({
        sameAsClient: '',
        name: '',
        address: '',
      }),
      projectAccess: this._formBuilder.group({
        parkingAvailability: [[]],
        projectLocation: [[]],
        productHandling: this._formBuilder.array([
          this._formBuilder.group({
            name: [{ value: 'Stairs', disabled: true }],
            dimensions: '',
          }),
          this._formBuilder.group({
            name: [{ value: 'Lift', disabled: true }],
            dimensions: '',
          }),
          this._formBuilder.group({
            name: [{ value: 'Handling by crane', disabled: true }],
            dimensions: '',
          }),
        ]),
        locationDescription: this._formBuilder.group({
          floor: '',
          ceiling: '',
          walls: '',
          heating: '',
          others: '',
        }),
      }),
    });

    this.newQuoteFormGroup.get('quoteRequest.company')?.valid
      ? this.newQuoteFormGroup.get('quoteRequest.client')?.enable()
      : this.newQuoteFormGroup.get('quoteRequest.client')?.disable();

    this.newQuoteFormGroup.get('quoteRequest.client')?.valid
      ? this.newQuoteFormGroup.get('quoteRequest.contact')?.enable()
      : this.newQuoteFormGroup.get('quoteRequest.contact')?.disable();

    this.newQuoteFormGroup
      .get('quoteRequest.company')
      ?.statusChanges.subscribe((status) => {
        status == 'VALID'
          ? this.newQuoteFormGroup.get('quoteRequest.client')?.enable()
          : this.newQuoteFormGroup.get('quoteRequest.client')?.disable();
      });

    this.newQuoteFormGroup
      .get('quoteRequest.client')
      ?.statusChanges.subscribe((status) => {
        status == 'VALID'
          ? this.newQuoteFormGroup.get('quoteRequest.contact')?.enable()
          : this.newQuoteFormGroup.get('quoteRequest.contact')?.disable();
      });

    let contactValueChanges$ = this.newQuoteFormGroup
      .get('quoteRequest.contact')
      ?.valueChanges.pipe(
        map((filter: string) => (filter ? filter.trim().toLowerCase() : '')),
        distinctUntilChanged()
      );

    this.filteredClients = combineLatest([
      this.clientFilter.valueChanges.pipe(
        map((filter: string) => (filter ? filter.trim().toLowerCase() : ''))
      ),
      this.clientsServ.clients$,
    ]).pipe(
      map(([filter, clients]: any) => {
        console.log(filter);
        console.log(clients);

        let clientIds = clients.map((client: Client) => client.id);

        console.log(clientIds);

        return clients;
      })
    );
  }

  // FormGroup Getter

  controlToGroup(controlName: string) {
    return this.newQuoteFormGroup.get(controlName) as FormGroup;
  }

  toFormGroup(abstractControl: AbstractControl) {
    return abstractControl as FormGroup;
  }

  toFormControl(abstractControl: AbstractControl | null): FormControl | null {
    if (abstractControl) return abstractControl as FormControl;

    return null;
  }

  toFormArray(abstractControl: AbstractControl) {
    return abstractControl as FormArray;
  }

  get client() {
    return this.newQuoteFormGroup.get('quoteRequest.client') as FormControl;
  }

  get floors(): FormArray {
    return this.newQuoteFormGroup.get('floors') as FormArray;
  }

  updateDistance(inc: boolean = true) {
    let val = this.newQuoteFormGroup.get('quote.distance')?.value;

    if (val !== null)
      this.newQuoteFormGroup
        .get('quote.distance')
        ?.setValue(inc ? val + 1 : val - 1);
  }

  addFloor() {
    this.floors.push(
      this._formBuilder.group({
        name: ['Floor', Validators.required],
        color: ['Grey Ral 9006', Validators.required],
        dimensions: ['', Validators.required],
      })
    );
  }

  removeFloor(i: number) {
    this.floors.removeAt(i);
  }

  getFloorHeightInMeters(floorHeight: FloorHeight) {
    let { feet, inches, fraction } = floorHeight;

    return (
      (feet || 0) / 3.281 +
      ((inches || 0) + (fraction || 0)) / 39.37
    ).toFixed(5);
  }

  get productHandling(): FormArray {
    return this.newQuoteFormGroup.get(
      'projectAccess.productHandling'
    ) as FormArray;
  }

  addProductHandling(el: Element) {
    this.productHandling.push(
      this._formBuilder.group({
        name: ['', [Validators.required]],
        dimensions: ['', [Validators.required]],
      })
    );
  }

  removeProductHandling(i: number) {
    this.productHandling.removeAt(i);
  }

  getProductHandlingDimensionsInMeters(phd: ProductHandlingDimensions) {
    let { length, width, height } = phd;

    let res = '';

    for (let d of Object.keys(phd)) {
      let val = (phd as any)[d];

      if (val) res = `${res} ${d.toUpperCase()}: ${(val / 3.281).toFixed(3)}m`;
    }

    return res;
  }

  // Quote Functions

  discardQuote() {
    this.matDialogRef.close();
    this._snackBar.open('Prospect discarded', '', { duration: 1000 });
  }

  saveQuote() {
    let update = false;

    if (this.data?.quote?.id) update = true;

    let quoteData = this.newQuoteFormGroup.value;

    this.matDialogRef.close({
      action: 'save',
      value: quoteData,
      quoteId: this.quoteId,
      update,
    });
  }

  toggleDialogView() {
    this.expandedView = !this.expandedView;

    this.expandedView
      ? this.matDialogRef.addPanelClass('expanded')
      : this.matDialogRef.removePanelClass('expanded');
  }
}

export function clientDNEValidator(
  clientsServ: ClientsService
): AsyncValidatorFn {
  return (
    control: AbstractControl
  ): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
    let clientName = control.value;

    if (!clientName) return of(null);
    else clientName = clientName.trim().toLowerCase();

    return clientsServ.clients$.pipe(
      take(1),
      map((clients: Client[]) => {
        return !clients.some(
          (client) =>
            `${client.basicDetails.fullName.firstName} ${client.basicDetails.fullName.lastName}`.toLowerCase() ==
            clientName
        )
          ? { clientDoesNotExist: true }
          : null;
      })
    );
  };
}

export class NewQuoteErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}
