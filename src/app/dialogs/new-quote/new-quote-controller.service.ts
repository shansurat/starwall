import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Quote } from 'src/app/interfaces/quote';
import { QuotesService } from 'src/app/services/quotes.service';
import { NewQuoteComponent } from './new-quote.component';

@Injectable({
  providedIn: 'root'
})
export class NewQuoteControllerService {

  constructor(
    private _matDialog: MatDialog,
    private _snackBar: MatSnackBar,
    private quotesServ: QuotesService
  ) { }

  openQuoteDialog(quote?: Quote) {
    let quoteDialogRef =
      this._matDialog.open(NewQuoteComponent, {
        panelClass: 'form-dialog',
        data: { quote },
      });

    quoteDialogRef.afterClosed().subscribe((res: any) => {
      if (res) {
        if (res.action == "save") {
          this.quotesServ.addQuote(res.value, res.quoteId, res.update)
            .then(() => this._snackBar.open('Prospect added', '', { duration: 1000 }))
            .catch(err => this._snackBar.open(err, '', { duration: 1000 }))
        } else if (res.action == "delete") {
          this.quotesServ.deleteQuote(res.clientId)
            .subscribe(() => {
              this._snackBar.open('Prospect deleted', '', { duration: 1000 })
            })
        }
      }
    })
  }
}
