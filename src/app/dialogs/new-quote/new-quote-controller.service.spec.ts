import { TestBed } from '@angular/core/testing';

import { NewQuoteControllerService } from './new-quote-controller.service';

describe('NewQuoteControllerService', () => {
  let service: NewQuoteControllerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NewQuoteControllerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
