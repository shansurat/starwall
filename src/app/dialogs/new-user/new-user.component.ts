import { ChangeDetectorRef, Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireFunctions } from '@angular/fire/functions';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from 'src/app/services/auth.service';

interface NewUserDialogData {
  expanded: boolean;
}

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NewUserComponent implements OnInit {
  expanded: boolean
  new_user__form: FormGroup;

  fullNameTextFields = [
    { name: 'lastName', type: 'text', required: true },
    { name: 'firstName', type: 'text', required: true },
    { name: 'middleName', type: 'text' },
  ]

  requiredFields = [
    { name: 'email', type: 'email', required: true },
    { name: 'username', type: 'text', required: true },
  ]

  constructor(
    public matDialogRef: MatDialogRef<NewUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: NewUserDialogData,
    private formBuilder: FormBuilder,
    protected changeDetectorRef: ChangeDetectorRef,
    private functions: AngularFireFunctions

  ) {
    this.expanded = data.expanded

    this.new_user__form = this.formBuilder.group({
      fullName: this.formBuilder.group({
        firstName: [''],
        lastName: [''],
        middleName: [''],
      }),
      email: [''],
      username: [''],
      admin: ['']
    })


  }

  ngOnInit(): void {
    this.changeDetectorRef.detectChanges();

  }

  toggleView() {
    this.expanded = !this.expanded
  }


  submitForm() {
    const addUser = this.functions.httpsCallable('addUser')

    addUser(this.new_user__form.value)
      .subscribe((res) => {
        console.log(res)
      })
  }
}
