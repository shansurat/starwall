import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate, CanActivateChild {
  constructor(
    private authServ: AuthService,
    private firestore: AngularFirestore
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.authServ.currentUser$
      .pipe(
        mergeMap(currentUser => {
          return this.firestore.collection('users').doc(currentUser?.uid).valueChanges()
        }),
        map((value: any) => {
          return value[0]?.admin
        }))

    return true;
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.authServ.currentUser$
      .pipe(
        mergeMap(currentUser => {
          return this.firestore.collection('users').doc(currentUser?.uid).valueChanges()
        }),
        map((value: any) => {
          return value[0]?.admin
        }))

    return true;
  }

}
