import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toolbarCase'
})
export class ToolbarCasePipe implements PipeTransform {

  transform(value: string): string {
    return value.replace('/', ' | ')
  }

}
