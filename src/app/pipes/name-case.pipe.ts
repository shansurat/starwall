import { Pipe, PipeTransform } from '@angular/core';

interface FullName {
  firstName: string;
  middleName?: string;
  lastName: string;
}

@Pipe({
  name: 'nameCase'
})
export class NameCasePipe implements PipeTransform {

  transform(fullName: FullName): string {
    let middleInitial = fullName.middleName ? fullName.middleName[0] : ''

    return [fullName.firstName, fullName.lastName].join(' ')
  }

}
