import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sidenavcase'
})
export class SidenavCasePipe implements PipeTransform {

  transform(value: string): string {
    return value.replace(/-/g, ' ');
  }

}
