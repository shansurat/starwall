export interface StarwallComponent {
  myReference: string;
  supplierReference: string;
  designation: string;
  purchasePrice: number;
  sellingPrice: number;
  company: string;
  supplier: string;
  family: string;
}
