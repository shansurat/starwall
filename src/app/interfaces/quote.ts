import { Client } from './client';
import { Company } from './company';
import { Representative } from './representative';
import firebase from 'firebase/app';

export interface Quote {
  id: string;
  number: string;
  createdAt: firebase.firestore.Timestamp;
  lastModifiedAt: firebase.firestore.Timestamp;
  quoteRequest: {
    clientId: string;
    projectName: string;
    date: Date;
    company: Company;
  };
  quote: {
    distance?: number;
    isLocalProject?: boolean;
    installationDate?: any;
    requestedDate?: any;
    work?: string[];
  };
  delivery: {
    address: string;
    name: string;
    sameAsClient: boolean;
  };
  floors: Floor[];
  projectAccess: {
    locationDescription?: {
      ceiling: string[];
      floor: string[];
      heating: string[];
      others: string[];
      walls: string[];
    };
    parkingAvailability?: string[];
    productHandling?: any[];
    projectLocation?: string[];
  };
}
export interface Floor {
  name: string;
  color: string;
  dimensions: any;
}
