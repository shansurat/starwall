export interface OrdersTableConfig {
  canHideColumns?: boolean;
  canReorderColumns?: boolean;
  sortable?: boolean;
  headerVisible?: boolean;
  displayedColumns: string[];
  availableColumns?: string[];
  actionsVisible?: boolean;
  canSelectRows?: boolean;
  paginator?: {
    length?: number;
    pageSize?: number;
    pageSizeOptions?: number[];
  };
}
