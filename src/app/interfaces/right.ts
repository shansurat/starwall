export interface Right {
  name: string;
  administrator: boolean;
  permissions: RightPermission[];
}

export interface RightPermission {
  name: string;
  access: string;
}
