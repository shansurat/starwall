import { Representative } from './representative';
import firebase from 'firebase/app';

export interface Client {
  id: string;
  accountDetails: {
    id: string;
    status: string;
    representative: Representative;
    distributor: { name: string };
    tags: string[];
  };
  basicDetails: {
    fullName: {
      firstName: string;
      lastName: string;
      middleName: string;
      suffix: string;
    };
    contact: {
      businessCards: File[];
      email: string;
      phoneNumber: string;
    };
    address: {
      fullAddress: string;
    };
  };
  noteAndReminders: {
    note?: string;
    reminders?: firebase.firestore.Timestamp[];
  };

  quoteDetails: {
    claimCredit?: boolean;
    note?: string;
    quote?: string;
  };
  createdAt: firebase.firestore.Timestamp;
  lastModifiedAt: firebase.firestore.Timestamp;
  contacts: ClientContact[];
  taxes: ClientTax[];
}

export interface ClientContact {
  name: string;
  title: string;
  telephone: string;
  email: string;
}

export interface ClientTax {
  name: string;
  value: string;
}
