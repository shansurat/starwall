export interface Employee {
  name: string;
  company: string;
  rights: string;
  employementDate: Date;
  telephone: string;
  email: string;
  username: string;
  password: string;
  note?: string;
}
