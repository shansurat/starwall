import { Client } from './client';
import { Company } from './company';

export interface Order {
  status: string;
  number: string;
  client: Client;
  company: Company;
  representative: string;
  creationDate: Date;
  installationDate: Date;
  aluminumDate: Date;
}
