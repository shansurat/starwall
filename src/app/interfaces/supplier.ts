export interface Supplier {
  name: string;
  manager: string;
  telephone: string;
  family: string;
}
