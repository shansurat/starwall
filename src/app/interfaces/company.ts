export interface Company {
  id: string;
  name: string;
  headOffice: string;
  financialClosingDate: Date;
  website: string;
  email: string;
  discount: {
    value: string;
    currency: string;
  };
  address: string;
  telephone: string;
  delivery: {
    email: string;
    address: string;
    telephone: string;
  };
  invoicing: {
    email: string;
    address: string;
    telephone: string;
  };
}
