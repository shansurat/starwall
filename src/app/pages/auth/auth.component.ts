import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service'

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  password_hidden: boolean = true
  signInForm: FormGroup

  constructor(
    private fb: FormBuilder,
    private auth: AuthService
  ) {
    this.signInForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      remember_me: ['']
    })
  }

  ngOnInit(): void {


  }

  signIn() {

    if (!this.auth.signIn(this.signInForm.value)) {
      this.signInForm.reset()
    }
  }

}
