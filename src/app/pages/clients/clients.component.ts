import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatSelectionList } from '@angular/material/list';
import { ClientsService } from 'src/app/services/clients.service';
import { MatDialog } from '@angular/material/dialog';
import { ClientComponent } from 'src/app/dialogs/new-client/new-client.component';
import { Client } from 'src/app/interfaces/client';
import { MatChipInputEvent } from '@angular/material/chips';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatSnackBar } from '@angular/material/snack-bar';

interface AdvancedFilterValue {
  field: string;
  value: string;
}

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss'],
})
export class ClientsComponent implements OnInit, AfterViewInit {
  spinnerShown!: boolean;
  progressBarShown: boolean = false;
  showCountsOnly: boolean = true;

  isAdvancedFilter = false;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  advancedFilterValues: AdvancedFilterValue[] = [];

  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSelectionList) displayedColumnsSelectionList!: MatSelectionList;

  dataSource = new MatTableDataSource<Client>([]);

  columns = [
    'status',
    'name',
    'tags',
    'address',
    'phoneNumber',
    'email',
    'representative',
    'distributor',
    'reminders',
    'createdAt',
    'lastModifiedAt',
    'contacts',
  ];

  displayedColumns: string[] = Object.assign([], this.columns);
  displayedColumnsSelected = Object.assign([], this.columns);

  constructor(
    public formBuilder: FormBuilder,
    public clientsServ: ClientsService,
    private _matDialog: MatDialog,
    private _snackBar: MatSnackBar,

  ) { }

  ngOnInit(): void {
    this.spinnerShown = true;
    this.clientsServ.clients$.subscribe((clients) => {
      this.dataSource.data = clients;
      this.spinnerShown = false;
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.filterPredicate = this.filterPredicate;
    this.dataSource.sortingDataAccessor = (
      client: Client | any,
      sortHeaderId: string
    ) => {
      switch (sortHeaderId) {
        case 'status':
          return client.accountDetails.status;
        case 'representative':
        case 'distributor':
          return client.accountDetails[sortHeaderId].name;
        case 'tags':
          return client.accountDetails.tags.length;
        case 'name':
          return `${client.basicDetails.fullName.firstName} ${client.basicDetails.fullName.lastName}`;
        case 'email':
        case 'phoneNumber':
          return client.basicDetails.contact[sortHeaderId];
        case 'address':
          return client.basicDetails.address.fullAddress;
        case 'reminders':
          return client.noteAndReminders.reminders.length;
        case 'contacts':
          return client.contacts.length;
        case 'createdAt':
        case 'lastModifiedAt':
          return client[sortHeaderId].toDate();

        default:
          break;
      }
    };
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.progressBarShown = false;
  }

  // Basic Filter
  applyBasicFilter(e: KeyboardEvent) {
    const basicFilterValue = (e.target as HTMLInputElement).value;
    this.dataSource.filter = basicFilterValue.trim().toLowerCase();
  }

  // Advanced Filter
  addAdvancedFilterValue(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.advancedFilterValues.push({
        field: value.split(':')[0],
        value: value.split(':')[1],
      });
    }

    if (input) {
      input.value = '';
    }
  }

  removeAdvancedFilterValue(advancedFilterValue: AdvancedFilterValue): void {
    const index = this.advancedFilterValues.indexOf(advancedFilterValue);

    if (index >= 0) this.advancedFilterValues.splice(index, 1);
  }

  filterPredicate(client: Client | any, filter: string) {
    if (client.accountDetails.status.includes(filter)) return true;

    return false;
  }

  // Drag and Drop
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(
      this.displayedColumns,
      event.previousIndex,
      event.currentIndex
    );
  }

  // Get Status Color
  getStatusColor(status: string) {
    const statusColors: any = {
      prospect: '#66bb6a',
      client: '#4fc3f7',
    };

    return statusColors[status];
  }

  hideColumn(column: string) {
    this.displayedColumnsSelected.splice(
      this.displayedColumnsSelected.indexOf(column),
      1
    );
    this.displayedColumnsSelectionList.writeValue(
      this.displayedColumnsSelected
    );
  }

  openClientDialog(client?: Client) {
    let clientDialogRef =
      this._matDialog.open(ClientComponent, {
        panelClass: 'form-dialog',
        data: { client },
      });

    clientDialogRef.afterClosed().subscribe((res: any) => {
      if (res) {
        this.progressBarShown = true
        if (res.action == "save") {
          this.clientsServ.addClient(res.value, res.clientId, res.update)
            .then(() => this._snackBar.open('Prospect added', '', { duration: 1000 }))
            .catch(err => this._snackBar.open(err, '', { duration: 1000 }))
            .finally(() => this.progressBarShown = false)
        } else if (res.action == "delete") {
          this.clientsServ.deleteClient(res.clientId)
            .subscribe(() => {
              this._snackBar.open('Prospect deleted', '', { duration: 1000 })
              this.progressBarShown = false
            })
        }
      }
    })
  }
}
