import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { Order } from 'src/app/interfaces/order';
import { OrdersService } from 'src/app/services/orders.service';
import { MatSelectionList } from '@angular/material/list';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
})
export class OrdersComponent implements OnInit, AfterViewInit {
  spinnerShown!: boolean;
  progressBarShown!: boolean;

  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSelectionList) dispalyedColumnsSelectionList!: MatSelectionList;

  dataSource: MatTableDataSource<Order> = new MatTableDataSource();

  columns = [
    'status',
    'number',
    'client',
    'creationDate',
    'installationDate',
    'aluminumDate',
    'company',
    'representative',
    'actions',
  ];

  displayedColumns: string[] = Object.assign([], this.columns);
  displayedColumnsSelected = Object.assign([], this.columns);
  orderStatuses = ['to-process', 'standby', 'in-process', 'completed'];

  // Table Config with default values
  defaultTableConfig = ['fixed_header'];
  tableConfig = new FormControl(this.defaultTableConfig);

  constructor(
    public formBuilder: FormBuilder,
    public ordersServ: OrdersService
  ) {}

  ngOnInit(): void {
    this.spinnerShown = true;
  }

  ngAfterViewInit() {
    this.ordersServ.orders$.subscribe((orders) => {
      this.dataSource.filterPredicate = this.filterPredicate;
      this.dataSource.sort = this.sort;
      this.dataSource.sortingDataAccessor = (
        data: Order | any,
        sortHeaderId: string
      ) => {
        let field = data[sortHeaderId];
        if (sortHeaderId == 'client' || sortHeaderId == 'company')
          return field.name;
        else if (sortHeaderId == 'status') {
          return this.orderStatuses.indexOf(field);
        }

        return field;
      };
      this.dataSource.paginator = this.paginator;

      this.dataSource.data = orders;

      this.spinnerShown = false;
    });
  }

  // Basic Filter
  applyBasicFilter(e: KeyboardEvent) {
    const basicFilterValue = (e.target as HTMLInputElement).value;
    this.dataSource.filter = basicFilterValue.trim().toLowerCase();
  }

  filterPredicate(data: Order | any, filter: string) {
    this.spinnerShown = true;

    for (let field in data) {
      let value = data[field];

      // If a client or a company field
      if (['client', 'company'].includes(field)) {
        if (value.name.toLowerCase().includes(filter)) {
          this.spinnerShown = false;
          return true;
        }
      }
      // If a date field
      else if (
        ['creationDate', 'installationDate', 'aluminumDate'].includes(field)
      ) {
        if (new Date(value).toDateString().toLowerCase().includes(filter)) {
          this.spinnerShown = false;
          return true;
        }
      }
      // If a status field
      else if (field == 'status') {
        if (value.split('-').join(' ').includes(filter)) {
          this.spinnerShown = false;
          return true;
        }
      } else {
        if (value.toLowerCase().includes(filter)) {
          this.spinnerShown = false;
          return true;
        }
      }
    }

    this.spinnerShown = false;
    return false;
  }

  // Drag and Drop
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(
      this.displayedColumns,
      event.previousIndex,
      event.currentIndex
    );
  }

  // Status variables and methods

  // Get Status Color
  getStatusColor(status: string) {
    const statusColors: any = {
      'in-process': '#ffb74d',
      'to-process': '#4fc3f7',
      completed: '#66bb6a',
      standby: '#ef5350',
    };

    return statusColors[status];
  }

  // Advanced Filter
  advancedFilters = [];
}
