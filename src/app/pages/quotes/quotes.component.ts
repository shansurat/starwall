import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Order } from 'src/app/interfaces/order';
import { MatSelectionList } from '@angular/material/list';
import { QuotesService } from 'src/app/services/quotes.service';
import { Quote } from 'src/app/interfaces/quote';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog } from '@angular/material/dialog';
import { NewQuoteControllerService } from 'src/app/dialogs/new-quote/new-quote-controller.service';
import { ClientsService } from 'src/app/services/clients.service';

interface AdvancedFilterValue {
  field: string;
  value: string;
}

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.scss'],
})
export class QuotesComponent implements OnInit, AfterViewInit {
  spinnerShown!: boolean;
  progressBarShown!: boolean;

  isAdvancedFilter!: boolean;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  advancedFilterValues: AdvancedFilterValue[] = [];

  isLegendShown!: boolean;

  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSelectionList) displayedColumnsSelectionList!: MatSelectionList;

  dataSource = new MatTableDataSource<Quote>();

  columns = [
    'status',
    'number',
    'client',
    'project',
    'date',
    'distributor',
    'representative',
  ];

  displayedColumns: string[] = Object.assign([], this.columns);
  displayedColumnsSelected = Object.assign([], this.columns);

  // Table Config with default values
  tableConfig = new FormControl([]);

  constructor(
    public quotesServ: QuotesService,
    public clientsServ: ClientsService,
    private _matDialog: MatDialog,
    public newQuoteController: NewQuoteControllerService
  ) {}

  ngOnInit(): void {
    this.spinnerShown = true;
    this.quotesServ.quotes$.subscribe((quotes) => {
      this.dataSource.data = quotes;
      this.spinnerShown = false;
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.filterPredicate = this.filterPredicate;
    this.dataSource.sort = this.sort;
    this.dataSource.sortingDataAccessor = (
      data: Order | any,
      sortHeaderId: string
    ) => {
      let field = data[sortHeaderId];
      if (['client', 'distributor', 'representative'].includes(sortHeaderId))
        return field.name;
      else if (sortHeaderId == 'status') {
        return this.quoteStatuses.indexOf(field);
      }

      return field;
    };
    this.dataSource.paginator = this.paginator;
  }

  // Basic Filter
  applyBasicFilter(e: KeyboardEvent) {
    const basicFilterValue = (e.target as HTMLInputElement).value;
    this.dataSource.filter = basicFilterValue.trim().toLowerCase();
  }

  // Add Advanced Filter
  addAdvancedFilterValue(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.advancedFilterValues.push({
        field: value.split(':')[0],
        value: value.split(':')[1],
      });
    }

    if (input) {
      input.value = '';
    }
  }

  // Remove Advanced Filter
  removeAdvancedFilterValue(advancedFilterValue: AdvancedFilterValue): void {
    const index = this.advancedFilterValues.indexOf(advancedFilterValue);

    if (index >= 0) this.advancedFilterValues.splice(index, 1);
  }

  filterPredicate(data: Quote | any, filter: string) {
    for (let field in data) {
      let value = data[field];

      // If a client, distributor, or a representative
      if (['client', 'distributor', 'representative'].includes(field)) {
        if (value.name.toLowerCase().includes(filter)) return true;
      }
      // If a date field
      else if (field == 'date') {
        if (new Date(value).toDateString().toLowerCase().includes(filter))
          return true;
      }
      // If a status field
      else if (field == 'status') {
        if (value.split('-').join(' ').includes(filter)) return true;
      } else {
        console.log(field, value);
        if (value.toLowerCase().includes(filter)) return true;
      }
    }

    return false;
  }

  // Drag and Drop
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(
      this.displayedColumns,
      event.previousIndex,
      event.currentIndex
    );
  }

  // Status variables and methods

  quoteStatuses = [
    'standby',
    'information-missing',
    'express',
    'estimating-department',
    '3d-model',
    'processed',
  ];

  // Get Status Color
  getStatusColor(status: string) {
    const statusColors: any = {
      'information-missing': '#ffb74d',
      'estimating-department': '#c0ca33',
      '3d-model': '#9575cd',
      processed: '#66bb6a',
      express: '#4fc3f7',
      standby: '#ef5350',
    };

    return statusColors[status];
  }

  hideColumn(column: string) {
    this.displayedColumnsSelected.splice(
      this.displayedColumnsSelected.indexOf(column),
      1
    );
    this.displayedColumnsSelectionList.writeValue(
      this.displayedColumnsSelected
    );

    console.log(this.displayedColumnsSelected);
  }
}
