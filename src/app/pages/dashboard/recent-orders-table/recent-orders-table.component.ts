import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { Order } from 'src/app/interfaces/order';
import { OrdersService } from 'src/app/services/orders.service';
import { MatSelectionList } from '@angular/material/list';

@Component({
  selector: 'app-recent-orders-table',
  templateUrl: './recent-orders-table.component.html',
  styleUrls: ['./recent-orders-table.component.scss'],
})
export class RecentOrdersTableComponent implements OnInit {
  spinnerShown!: boolean;
  progressBarShown!: boolean;

  recentOrdersNumber: number = 5;

  @ViewChild(MatSelectionList) dispalyedColumnsSelectionList!: MatSelectionList;

  dataSource!: MatTableDataSource<Order>;

  columns = ['status', 'number', 'client', 'representative'];

  displayedColumns: string[] = Object.assign([], this.columns);
  displayedColumnsSelected = Object.assign([], this.columns);
  orderStatuses = ['to-process', 'standby', 'in-process', 'completed'];

  constructor(
    public formBuilder: FormBuilder,
    public ordersServ: OrdersService
  ) {}

  ngOnInit(): void {
    this.spinnerShown = true;
    this.ordersServ.orders$.subscribe((orders) => {
      this.dataSource = new MatTableDataSource(
        orders.slice(0, this.recentOrdersNumber)
      );
      this.spinnerShown = false;
    });
  }

  // Drag and Drop
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(
      this.displayedColumns,
      event.previousIndex,
      event.currentIndex
    );
  }

  // Get Status Color
  getStatusColor(status: string) {
    const statusColors: any = {
      'in-process': '#ffb74d',
      'to-process': '#4fc3f7',
      completed: '#66bb6a',
      standby: '#ef5350',
    };

    return statusColors[status];
  }
}
