import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatSelectionList } from '@angular/material/list';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Quote } from 'src/app/interfaces/quote';
import { QuotesService } from 'src/app/services/quotes.service';

@Component({
  selector: 'app-recent-quotes-table',
  templateUrl: './recent-quotes-table.component.html',
  styleUrls: ['./recent-quotes-table.component.scss'],
})
export class RecentQuotesTableComponent implements OnInit, AfterViewInit {
  spinnerShown!: boolean;
  progressBarShown!: boolean;

  recentQuotesNumber: number = 5;

  @ViewChild(MatSelectionList) dispalyedColumnsSelectionList!: MatSelectionList;

  dataSource!: MatTableDataSource<Quote>;

  columns = ['status', 'number', 'client', 'representative'];

  displayedColumns: string[] = Object.assign([], this.columns);
  displayedColumnsSelected = Object.assign([], this.columns);

  // Table Config with default values
  defaultTableConfig = ['fixed_header'];
  tableConfig = new FormControl(this.defaultTableConfig);
  constructor(
    public formBuilder: FormBuilder,
    public quotesServ: QuotesService
  ) {}

  ngOnInit(): void {
    this.spinnerShown = true;
    this.quotesServ.quotes$.subscribe((quotes) => {
      this.dataSource = new MatTableDataSource(
        quotes.slice(0, this.recentQuotesNumber)
      );

      this.spinnerShown = false;
    });
  }

  ngAfterViewInit(): void {}

  // Drag and Drop
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(
      this.displayedColumns,
      event.previousIndex,
      event.currentIndex
    );
  }

  // Status variables and methods

  quoteStatuses = [
    'standby',
    'information-missing',
    'express',
    'estimating-department',
    '3d-model',
    'processed',
  ];

  // Get Status Color
  getStatusColor(status: string) {
    const statusColors: any = {
      'information-missing': '#ffb74d',
      'estimating-department': '#c0ca33',
      '3d-model': '#9575cd',
      processed: '#66bb6a',
      express: '#4fc3f7',
      standby: '#ef5350',
    };

    return statusColors[status];
  }
}
