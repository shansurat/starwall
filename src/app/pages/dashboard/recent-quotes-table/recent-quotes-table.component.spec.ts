import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentQuotesTableComponent } from './recent-quotes-table.component';

describe('RecentQuotesTableComponent', () => {
  let component: RecentQuotesTableComponent;
  let fixture: ComponentFixture<RecentQuotesTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecentQuotesTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentQuotesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
