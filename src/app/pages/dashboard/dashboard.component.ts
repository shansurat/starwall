import {
  CdkDragDrop,
  CdkDropList,
  CdkDropListGroup,
  copyArrayItem,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import {
  AfterContentChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
  ViewChildren,
  ViewEncapsulation,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, MatSortable } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { BehaviorSubject } from 'rxjs';
import { Order } from 'src/app/interfaces/order';
import { Quote } from 'src/app/interfaces/quote';
import { ClientsService } from 'src/app/services/clients.service';
import { OrdersService } from 'src/app/services/orders.service';
import { QuotesService } from 'src/app/services/quotes.service';
import {
  CompactType,
  DisplayGrid,
  GridsterConfig,
  GridsterItem,
  GridType,
} from 'angular-gridster2';

interface DashboardDragDropComponent {
  name: string;
  type?: string;
  title?: any;
  sections?: DashboardDragDropComponent[][];
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent
  implements OnInit, AfterViewInit, AfterContentChecked {
  options!: GridsterConfig;
  dashboard!: any;

  editDashboardMode: boolean = false;

  width!: number;

  availableComponents = [
    { name: 'counter', type: 'quote', title: 'Quote Counter' },
    { name: 'counter', type: 'client', title: 'Client Counter' },
    { name: 'counter', type: 'order', title: 'Order Counter' },
    { name: 'calendar', title: 'Calendar' },
    { name: 'recent-quotes', title: 'Recent Quotes Table' },
    { name: 'recent-orders', title: 'Recent Orders Table' },
    { name: 'summary-statistics', title: 'Summary Statistics' },
  ];

  components = [
    {
      name: 'counter',
      type: 'quote',
      title: 'Quote Counter',
      coor: {
        cols: 8,
        rows: 2,
        y: 0,
        x: 0,
      },
    },

    {
      name: 'counter',
      type: 'client',
      title: 'Client Counter',
      coor: {
        cols: 8,
        rows: 2,
        y: 0,
        x: 2,
      },
    },

    {
      name: 'counter',
      type: 'order',
      title: 'Order Counter',
      coor: {
        cols: 8,
        rows: 2,
        y: 0,
        x: 4,
      },
    },

    {
      name: 'calendar',
      title: 'Calendar',
      coor: {
        cols: 12,
        rows: 9,
        y: 1,
        x: 0,
      },
    },

    {
      name: 'summary-statistics',
      title: 'Summary Statistics',
      coor: {
        cols: 12,
        rows: 9,
        y: 1,
        x: 12,
      },
    },
    {
      name: 'recent-quotes',
      title: 'Recent Quotes Table',
      coor: {
        cols: 12,
        rows: 7,
        y: 10,
        x: 0,
      },
    },
    {
      name: 'recent-quotes',
      title: 'Recent Quotes Table',
      coor: {
        cols: 12,
        rows: 7,
        y: 10,
        x: 12,
      },
    },
  ];

  constructor(
    public quotesServ: QuotesService,
    public ordersServ: OrdersService,
    public clientsServ: ClientsService
  ) { }

  static itemChange(item: any, itemComponent: any) {
    console.info('itemChanged', item, itemComponent);
  }

  static itemResize(item: any, itemComponent: any) {
    console.info('itemResized', item, itemComponent);
  }

  ngOnInit() {
    this.options = {
      itemChangeCallback: DashboardComponent.itemChange,
      itemResizeCallback: DashboardComponent.itemResize,
      gridType: GridType.ScrollVertical,
      compactType: CompactType.None,
      margin: 16,
      outerMargin: true,
      outerMarginTop: 16,
      outerMarginRight: 16,
      outerMarginBottom: 16,
      outerMarginLeft: 16,
      useTransformPositioning: true,
      mobileBreakpoint: 640,
      minCols: 24,
      maxCols: 24,
      minRows: 24,
      maxRows: 100,
      maxItemCols: 24,
      minItemCols: 1,
      maxItemRows: 24,
      minItemRows: 1,
      maxItemArea: 2500,
      minItemArea: 1,
      defaultItemCols: 1,
      defaultItemRows: 1,
      fixedColWidth: 105,
      fixedRowHeight: 105,
      keepFixedHeightInMobile: false,
      keepFixedWidthInMobile: false,
      scrollSensitivity: 10,
      scrollSpeed: 20,
      enableEmptyCellClick: false,
      enableEmptyCellContextMenu: false,
      enableEmptyCellDrop: false,
      enableEmptyCellDrag: false,
      enableOccupiedCellDrop: false,
      emptyCellDragMaxCols: 50,
      emptyCellDragMaxRows: 50,
      ignoreMarginInRow: false,
      draggable: {
        enabled: true,
      },
      resizable: {
        enabled: true,
      },
      swap: true,
      pushItems: true,
      disablePushOnDrag: false,
      disablePushOnResize: false,
      pushDirections: { north: true, east: true, south: true, west: true },
      pushResizeItems: true,
      displayGrid: DisplayGrid.None,
      disableWindowResize: false,
      disableWarnings: false,
      scrollToNewItems: true,
    };
  }

  changedOptions() {
    if (this.options.api?.optionsChanged) this.options.api.optionsChanged();
  }

  removeItem(item: any) {
    this.dashboard.splice(this.dashboard.indexOf(item), 1);
  }

  addItem() {
    this.dashboard.push({ cols: 2, rows: 1, y: 0, x: 0 });
  }

  ngAfterViewInit(): void { }

  ngAfterContentChecked(): void {
    //Called after every check of the component's or directive's content.
    //Add 'implements AfterContentChecked' to the class.
  }
}
