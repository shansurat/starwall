import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotesStatisticsComponent } from './quotes-statistics.component';

describe('QuotesStatisticsComponent', () => {
  let component: QuotesStatisticsComponent;
  let fixture: ComponentFixture<QuotesStatisticsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuotesStatisticsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuotesStatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
