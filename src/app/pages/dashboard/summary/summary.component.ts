import {
  Component,
  DoCheck,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { fromEvent, Observable, Subscription } from 'rxjs';

@Component({
  selector: 'dashboard-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
})
export class SummaryComponent implements OnInit {
  public lineChartData: ChartDataSets[] = [
    { data: [], label: 'Quotes' },
    { data: [], label: 'Orders' },
  ];
  public lineChartLabels: Label[] = [];

  public lineChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [
        {
          gridLines: {
            display: false,
          },
          ticks: {
            fontColor: 'rgba(255,255,255,.75)', // y axes numbers color (can be hexadecimal too)
          },
        },
      ],
      yAxes: [
        {
          ticks: {
            fontColor: 'rgba(255,255,255,.75)', // y axes numbers color (can be hexadecimal too)
          },
          gridLines: {
            display: false, // grid line color (can be removed or changed)
          },
        },
      ],
    },
    legend: {
      labels: {
        fontColor: 'white',
      },
    },
  };
  public lineChartColors: Color[] = [
    {
      borderColor: '#009688',
      // backgroundColor: 'rgba(0,150,136,0.5)',
    },
    {
      borderColor: '#ffc107',
      // backgroundColor: 'rgba(255,193,7,.5)',
    },
  ];
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [];

  constructor(public renderer: Renderer2) {
    let i = 1;
    while (i <= 28) {
      this.lineChartData[0]?.data?.push(
        Math.floor(Math.random() * (100 - 50 + 1)) + 50
      );
      this.lineChartData[1]?.data?.push(
        Math.floor(Math.random() * (200 - 50 + 1)) + 50
      );

      this.lineChartLabels.push(`Feb ${i}`);
      i++;
    }
  }

  ngOnInit() { }
}
