import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FramesManagementComponent } from './frames-management.component';

describe('FramesManagementComponent', () => {
  let component: FramesManagementComponent;
  let fixture: ComponentFixture<FramesManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FramesManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FramesManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
