import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralParametersManagementComponent } from './general-parameters-management.component';

describe('GeneralParametersManagementComponent', () => {
  let component: GeneralParametersManagementComponent;
  let fixture: ComponentFixture<GeneralParametersManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GeneralParametersManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralParametersManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
