import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatSelectionList } from '@angular/material/list';
import { EmployeesService } from 'src/app/services/employees.service';
import { MatDialog } from '@angular/material/dialog';
import { Client } from 'src/app/interfaces/client';
import { MatChipInputEvent } from '@angular/material/chips';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Employee } from 'src/app/interfaces/employee';

interface AdvancedFilterValue {
  field: string;
  value: string;
}

@Component({
  selector: 'app-employees-management',
  templateUrl: './employees-management.component.html',
  styleUrls: ['./employees-management.component.scss'],
})
export class EmployeesManagementComponent implements OnInit {
  spinnerShown!: boolean;
  progressBarShown!: boolean;

  isAdvancedFilter = false;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  advancedFilterValues: AdvancedFilterValue[] = [];

  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSelectionList) displayedColumnsSelectionList!: MatSelectionList;

  dataSource = new MatTableDataSource<Employee>([]);

  columns = [
    'name',
    'company',
    'rights',
    'employmentDate',
    'telephone',
    'email',
    'username',
    'password',
    'note',
  ];

  displayedColumns: string[] = Object.assign([], this.columns);
  displayedColumnsSelected = [
    'name',
    'company',
    'rights',
    'telephone',
    'email',
  ];

  constructor(
    public formBuilder: FormBuilder,
    public employeesServ: EmployeesService,
    public matDialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.spinnerShown = true;
    this.employeesServ.employees$.subscribe((employees) => {
      this.dataSource.data = employees;
      this.spinnerShown = false;
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.filterPredicate = this.filterPredicate;
    this.dataSource.sortingDataAccessor = (
      data: Client | any,
      sortHeaderId: string
    ) => {
      let field = data[sortHeaderId];
      if (['distributor', 'representative'].includes(sortHeaderId))
        return field.name;

      return field;
    };
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.progressBarShown = false;
  }

  // Basic Filter
  applyBasicFilter(e: KeyboardEvent) {
    const basicFilterValue = (e.target as HTMLInputElement).value;
    this.dataSource.filter = basicFilterValue.trim().toLowerCase();
  }

  // Advanced Filter
  addAdvancedFilterValue(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.advancedFilterValues.push({
        field: value.split(':')[0],
        value: value.split(':')[1],
      });
    }

    if (input) {
      input.value = '';
    }
  }

  removeAdvancedFilterValue(advancedFilterValue: AdvancedFilterValue): void {
    const index = this.advancedFilterValues.indexOf(advancedFilterValue);

    if (index >= 0) this.advancedFilterValues.splice(index, 1);
  }

  filterPredicate(data: Client | any, filter: string) {
    for (let field in data) {
      let value = data[field];

      // If a client or a company field
      if (['distributor', 'representative'].includes(field)) {
        if (value.name.toLowerCase().includes(filter)) return true;
      }
      // If a date field
      else if (
        ['reminder', 'creationDate', 'modificationDate'].includes(field)
      ) {
        if (new Date(value).toDateString().toLowerCase().includes(filter))
          return true;
      } else {
        if (value.toLowerCase().includes(filter)) return true;
      }
    }

    return false;
  }

  // Drag and Drop
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(
      this.displayedColumns,
      event.previousIndex,
      event.currentIndex
    );
  }

  // Get Status Color
  getStatusColor(status: string) {
    const statusColors: any = {
      prospect: '#66bb6a',
      client: '#4fc3f7',
    };

    return statusColors[status];
  }

  hideColumn(column: string) {
    this.displayedColumnsSelected.splice(
      this.displayedColumnsSelected.indexOf(column),
      1
    );
    this.displayedColumnsSelectionList.writeValue(
      this.displayedColumnsSelected
    );
  }

  openNewClientDialog() {}
}
