import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatSelectionList } from '@angular/material/list';
import { MatDialog } from '@angular/material/dialog';
import { MatChipInputEvent } from '@angular/material/chips';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { CompaniesService } from 'src/app/services/companies.service';
import { Company } from 'src/app/interfaces/company';
import { Supplier } from 'src/app/interfaces/supplier';
import { SuppliersService } from 'src/app/services/suppliers.service';

interface AdvancedFilterValue {
  field: string;
  value: string;
}

@Component({
  selector: 'app-suppliers-management',
  templateUrl: './suppliers-management.component.html',
  styleUrls: ['./suppliers-management.component.scss'],
})
export class SuppliersManagementComponent implements OnInit {
  spinnerShown!: boolean;
  progressBarShown!: boolean;

  isAdvancedFilter = false;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  advancedFilterValues: AdvancedFilterValue[] = [];

  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSelectionList) displayedColumnsSelectionList!: MatSelectionList;

  dataSource = new MatTableDataSource<Supplier>([]);

  columns = ['name', 'manager', 'telephone', 'family'];

  displayedColumns: string[] = Object.assign([], this.columns);
  displayedColumnsSelected: string[] = Object.assign([], this.columns);

  constructor(
    public formBuilder: FormBuilder,
    public suppliersServ: SuppliersService,
    public matDialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.spinnerShown = true;
    this.suppliersServ.suppliers$.subscribe((suppliers) => {
      this.dataSource.data = suppliers;
      this.spinnerShown = false;
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.filterPredicate = this.filterPredicate;
    this.dataSource.sortingDataAccessor = (
      data: Company | any,
      sortHeaderId: string
    ) => {
      let field = data[sortHeaderId];
      if (['distributor', 'representative'].includes(sortHeaderId))
        return field.name;

      return field;
    };
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.progressBarShown = false;
  }

  // Basic Filter
  applyBasicFilter(e: KeyboardEvent) {
    const basicFilterValue = (e.target as HTMLInputElement).value;
    this.dataSource.filter = basicFilterValue.trim().toLowerCase();
  }

  // Advanced Filter
  addAdvancedFilterValue(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.advancedFilterValues.push({
        field: value.split(':')[0],
        value: value.split(':')[1],
      });
    }

    if (input) {
      input.value = '';
    }
  }

  removeAdvancedFilterValue(advancedFilterValue: AdvancedFilterValue): void {
    const index = this.advancedFilterValues.indexOf(advancedFilterValue);

    if (index >= 0) this.advancedFilterValues.splice(index, 1);
  }

  filterPredicate(data: Company | any, filter: string) {
    for (let field in data) {
      let value = data[field];
      console.log(value);
      // If a date field
      if (field == 'financialClosingDate') {
        if (new Date(value).toDateString().toLowerCase().includes(filter))
          return true;
      } else if (field == 'discount') {
        if (`${value.value}% | ${value.currency}`.includes(filter)) return true;
      } else if (['delivery', 'invoicing'].includes(field)) {
        if (value.email.includes(filter)) return true;
      } else {
        if (value.toLowerCase().includes(filter)) return true;
      }
    }

    return false;
  }

  // Drag and Drop
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(
      this.displayedColumns,
      event.previousIndex,
      event.currentIndex
    );
  }

  // Get Status Color
  getStatusColor(status: string) {
    const statusColors: any = {
      prospect: '#66bb6a',
      client: '#4fc3f7',
    };

    return statusColors[status];
  }

  hideColumn(column: string) {
    this.displayedColumnsSelected.splice(
      this.displayedColumnsSelected.indexOf(column),
      1
    );
    this.displayedColumnsSelectionList.writeValue(
      this.displayedColumnsSelected
    );
  }

  openNewClientDialog() {}
}
