import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentsManagementComponent } from './components-management.component';

describe('ComponentsManagementComponent', () => {
  let component: ComponentsManagementComponent;
  let fixture: ComponentFixture<ComponentsManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponentsManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentsManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
