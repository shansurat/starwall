import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatSelectionList } from '@angular/material/list';
import { MatDialog } from '@angular/material/dialog';
import { MatChipInputEvent } from '@angular/material/chips';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { StarwallComponent } from 'src/app/interfaces/component';
import { ComponentsService } from 'src/app/services/components.service';

interface AdvancedFilterValue {
  field: string;
  value: string;
}

@Component({
  selector: 'app-components-management',
  templateUrl: './components-management.component.html',
  styleUrls: ['./components-management.component.scss'],
})
export class ComponentsManagementComponent implements OnInit {
  spinnerShown!: boolean;
  progressBarShown!: boolean;

  isAdvancedFilter = false;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  advancedFilterValues: AdvancedFilterValue[] = [];

  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSelectionList) displayedColumnsSelectionList!: MatSelectionList;

  dataSource = new MatTableDataSource<StarwallComponent>([]);

  columns = [
    'myReference',
    'supplierReference',
    'designation',
    'purchasePrice',
    'sellingPrice',
    'company',
    'supplier',
    'family',
  ];

  displayedColumns: string[] = Object.assign([], this.columns);
  displayedColumnsSelected: string[] = Object.assign([], this.columns);

  constructor(
    public formBuilder: FormBuilder,
    public componentsServ: ComponentsService,
    public matDialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.spinnerShown = true;
    this.componentsServ.components$.subscribe((components) => {
      this.dataSource.data = components;
      this.spinnerShown = false;
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.filterPredicate = this.filterPredicate;
    this.dataSource.sortingDataAccessor = (
      data: StarwallComponent | any,
      sortHeaderId: string
    ) => {
      let field = data[sortHeaderId];
      if (['distributor', 'representative'].includes(sortHeaderId))
        return field.name;

      return field;
    };
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.progressBarShown = false;
  }

  // Basic Filter
  applyBasicFilter(e: KeyboardEvent) {
    const basicFilterValue = (e.target as HTMLInputElement).value;
    this.dataSource.filter = basicFilterValue.trim().toLowerCase();
  }

  // Advanced Filter
  addAdvancedFilterValue(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.advancedFilterValues.push({
        field: value.split(':')[0],
        value: value.split(':')[1],
      });
    }

    if (input) {
      input.value = '';
    }
  }

  removeAdvancedFilterValue(advancedFilterValue: AdvancedFilterValue): void {
    const index = this.advancedFilterValues.indexOf(advancedFilterValue);

    if (index >= 0) this.advancedFilterValues.splice(index, 1);
  }

  filterPredicate(data: StarwallComponent | any, filter: string) {
    for (let field in data) {
      let value = data[field];
      console.log(value);
      // If a date field
      if (['sellingPrice', 'purchasePrice'].includes(field)) {
        if ((value as number).toString().includes(filter)) return true;
      } else {
        if (value.toLowerCase().includes(filter)) return true;
      }
    }

    return false;
  }

  // Drag and Drop
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(
      this.displayedColumns,
      event.previousIndex,
      event.currentIndex
    );
  }

  // Get Status Color
  getStatusColor(status: string) {
    const statusColors: any = {
      prospect: '#66bb6a',
      client: '#4fc3f7',
    };

    return statusColors[status];
  }

  hideColumn(column: string) {
    this.displayedColumnsSelected.splice(
      this.displayedColumnsSelected.indexOf(column),
      1
    );
    this.displayedColumnsSelectionList.writeValue(
      this.displayedColumnsSelected
    );
  }

  openNewClientDialog() {}
}
