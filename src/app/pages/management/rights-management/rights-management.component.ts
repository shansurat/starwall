import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RightsService } from 'src/app/services/rights.service';
import { Right, RightPermission } from 'src/app/interfaces/right';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
} from '@angular/forms';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-rights-management',
  templateUrl: './rights-management.component.html',
  styleUrls: ['./rights-management.component.scss'],
})
export class RightsManagementComponent implements OnInit {
  selectedRightFormControl: FormControl = new FormControl();
  rights!: Right[];

  selectedRightFormGroup!: FormGroup;

  constructor(
    public rightsServ: RightsService,
    public matDialog: MatDialog,
    private _formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.rightsServ.rights$.subscribe((rights) => (this.rights = rights));

    this.selectedRightFormControl.valueChanges.subscribe((value: Right[]) => {
      console.log(value);

      const selectedRight = value[0];
      this.selectedRightFormGroup = this._formBuilder.group({
        name: selectedRight.name,
        administrator: selectedRight.administrator,
        permissions: this._formBuilder.array([]),
      });

      selectedRight.permissions.forEach((permission) =>
        this.addPermission(permission)
      );
    });
  }

  toFormGroup(abstractControl: AbstractControl) {
    return abstractControl as FormGroup;
  }

  toFormControl(abstractControl: AbstractControl | null): FormControl | null {
    if (abstractControl) return abstractControl as FormControl;

    return null;
  }

  toFormArray(abstractControl: AbstractControl) {
    return abstractControl as FormArray;
  }

  addRight() {
    this.rights.push();
  }

  get permissions() {
    return this.selectedRightFormGroup.get('permissions') as FormArray;
  }

  addPermission(defaultVal: RightPermission) {
    this.permissions.push(
      this._formBuilder.group({
        name: new FormControl(defaultVal.name),
        access: new FormControl(defaultVal.access),
      })
    );
  }

  openNewClientDialog() {}
}
