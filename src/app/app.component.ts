import { animate, style, transition, trigger } from '@angular/animations';
import { OverlayContainer } from '@angular/cdk/overlay';
import {
  Component,
  HostBinding,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import {
  Event,
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router,
} from '@angular/router';
import { AuthService } from './services/auth.service';
import { LayoutService } from './services/layout.service';
import { MatIconService } from './services/mat-icon.service';

interface SidenavLink {
  name: string;
  admin?: boolean;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('sidenavLink', [
      transition(':enter', [
        style({ opacity: 0, height: 0 }),
        animate('250ms', style({ opacity: '*', height: '*' })),
      ]),
      transition(':leave', [
        animate('250ms', style({ opacity: 0, height: 0 })),
      ]),
    ]),
  ],
})
export class AppComponent implements OnInit {
  title = 'Starwall';
  avatarIsOpen = false;
  @HostBinding('class') className = '';
  isLoading = false;

  headersVisibility = {
    overview: true,
    management: true,
  };

  // Should be an observable
  langs: any[] = [{ name: 'English', enabled: true }, { name: 'Français' }];

  sidenav_links = {
    overview: [
      'dashboard',
      'clients',
      'quotes',
      'orders',
      'installations',
      'statistics',
    ],
    management: [
      'employees',
      'companies',
      'rights',
      'components',
      'general-parameters',
      'suppliers',
    ],
  };

  // Material Icons Array
  mat_icons = [
    'account-settings',
    'admin-tools',
    'check',
    'dark-mode',
    'edit',
    'filter_active',
    'filter',
    'global-settings',
    'language',
    'management_colored',
    'notification_active',
    'notification',
    'remove',
    'search',
    'sign-out',
  ];

  constructor(
    public authServ: AuthService,
    public router: Router,
    matIconServ: MatIconService,
    private overlay: OverlayContainer,
    public layoutServ: LayoutService
  ) {
    // Loading material icons
    this.mat_icons.forEach((mat_icon) => {
      matIconServ.makeMatIcon(mat_icon);
    });

    this.sidenav_links.overview.forEach((mat_icon) => {
      matIconServ.makeMatIcon(mat_icon);
      matIconServ.makeMatIcon(`${mat_icon}_active`);
      matIconServ.makeMatIcon(`${mat_icon}_colored`);
    });

    this.sidenav_links.management.forEach((mat_icon) => {
      mat_icon += '-management';
      matIconServ.makeMatIcon(mat_icon);
      matIconServ.makeMatIcon(`${mat_icon}_active`);
    });

    layoutServ.isHandset$.subscribe((res) => console.log('IsHandset: ', res));

    this.router.events.subscribe((event: Event) => {
      switch (true) {
        case event instanceof NavigationStart: {
          this.isLoading = true;
          break;
        }

        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: {
          this.isLoading = false;
          break;
        }
        default: {
          break;
        }
      }
    });
  }

  ngOnInit(): void {}
}
