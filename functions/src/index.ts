import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import serviceAccount from "./serviceAccountKey.json";

admin.initializeApp({
  credential: admin.credential.cert(<admin.ServiceAccount>serviceAccount),
});

const fncs = functions.region("europe-west1");

export const addUser = fncs.https.onCall((data, context) => {
  return data;
});

// Client Functions
export const deleteClient = fncs.https.onCall((data, context) => {
  admin.firestore().collection("clients").doc(data.id).delete()
      .then(() => functions.logger.info("DELETED"))
      .catch((e) => functions.logger.error(e));
});
